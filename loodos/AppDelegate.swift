//
//  AppDelegate.swift
//  loodos
//
//  Created by Ahmet Selman Durak on 14.02.2022.
//

import UIKit
import Firebase
import FirebaseRemoteConfig
import FirebaseMessaging
import Reachability


@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    //removed scene delegate that comes with ios start project template. This is done for supporting before ios 13 devices
    var window: UIWindow?
    let reachability = try! Reachability()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //init firebase
        FirebaseApp.configure()
        initFirebaseRemoteConfig()
        
        //start reachability notifier
        initReachabilityNotifier()
        
        //set status bar light
        application.statusBarStyle = .lightContent
        
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func initFirebaseRemoteConfig() {
        let remoteConfig = RemoteConfig.remoteConfig()
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings
        remoteConfig.setDefaults(fromPlist: "RemoteConfigDefaults")
    }
    
    func initReachabilityNotifier(){
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
    func getPermissonForPushNotifications(application: UIApplication) {
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: { _, _ in }
          )
        } else {
          let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()
    }
    

    
    //MARK: Reachability changed notifier function
    @objc func reachabilityChanged(note: Notification) {
        if reachability.connection == .unavailable {
            Utils.showConnectionErrorAlert()
        }
    }
    
}

