//
//  MovieDetailTableViewCell.swift
//  loodos
//
//  Created by Ahmet Selman Durak on 17.02.2022.
//

import UIKit

class MovieDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var icon : UIImageView!
    @IBOutlet weak var label : UILabel!
    
    override func awakeFromNib() {
        label.textColor = .white
        self.backgroundColor = .clear
        self.selectionStyle = .none
    }

    func fillCell(iconname : String, title:String, detail:String){
        icon.image = UIImage(named: iconname)
        label.setCustomAttributedText(string1: title,
                                      font1: FontBook.bold.of(size: 17),
                                      color1: Constants.colorPurple,
                                      string2: detail,
                                      font2: FontBook.semiBold.of(size: 17),
                                      color2: Constants.colroDarkGray)
    }
    

}
