//
//  AlertService.swift
//  Custom Alerts
//
//  Created by Ahmet Selman


import UIKit


class AlertService {
    
    func alert(title:String, body: String, actionButtonTitle : String, cancelButtonTitle:String? = nil,dismissOnTap:Bool? = false, confirmButtonTapped: (() -> Void)? = nil,  cancelButtonTapped: (() -> Void)? = nil) -> AlertViewController {
        
        let storyboard = UIStoryboard(name: "AlertStoryboard", bundle: .main)
        
        let alertVC = storyboard.instantiateViewController(withIdentifier: "AlertVC") as! AlertViewController
                
        alertVC.alertBody = body
        
        alertVC.actionButtonTitle = actionButtonTitle
        
        alertVC.cancelButtonTitle = cancelButtonTitle ?? nil
        
        alertVC.buttonAction = confirmButtonTapped
        
        alertVC.cancelAction = cancelButtonTapped
        
        alertVC.titleString = title
        
        alertVC.dismissOnTap = dismissOnTap ?? false
        
        return alertVC
    }
}



