//
//  AlertViewController.swift
//  Custom Alerts
//
//  Created by Ahmet Selman



import UIKit


class AlertViewController: UIViewController {

    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var imgType: UIImageView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var containerView: UIView!
        
    var alertBody = String()
    
    var actionButtonTitle : String!
    
    var cancelButtonTitle : String?
    
    var titleString : String!
                
    var buttonAction: (() -> Void)?
    
    var cancelAction: (() -> Void)?
    
    var dismissOnTap = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
    }

    func setupView() {
        
        view.backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.sendSubviewToBack(blurEffectView)

        bodyLabel.text = alertBody
        lblTitle.text = titleString
        
        if cancelButtonTitle != nil {
            cancelButton.setTitle(cancelButtonTitle!, for: .normal)
        }
        
        actionButton.setTitle(actionButtonTitle, for: .normal)
        
        
        if cancelButtonTitle == nil {
            self.cancelButton.isHidden = true
        }
        else {
            self.cancelButton.isHidden = false
        }
    }
    @IBAction func dismissTapped(_ sender: Any) {
        if dismissOnTap {
            dismiss(animated: true)
        }
        
    }
    
    @IBAction func didTapCancel(_ sender: Any) {
       
        dismiss(animated: true)
        cancelAction?()
    }
    
    
    @IBAction func didTapActionButton(_ sender: Any) {
        
        dismiss(animated: true)
        buttonAction?()
    }
    
}
