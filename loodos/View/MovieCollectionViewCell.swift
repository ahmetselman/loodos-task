//
//  MovieCollectionViewCell.swift
//  loodos
//
//  Created by Ahmet Selman Durak on 15.02.2022.
//

import UIKit
import Kingfisher

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgMovie : UIImageView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblImdbRating : UILabel!
    
    override func awakeFromNib() {
        imgMovie.backgroundColor = Constants.colorLightGray
        imgMovie.contentMode = .scaleAspectFill
        lblName.font = FontBook.bold.of(size: 15)
        lblImdbRating.font = FontBook.semiBold.of(size: 13)
    }
    
    func fillCell(film : Film) {
        if let imageUrl = URL(string: film.poster){
            imgMovie.kf.setImage(with: imageUrl)
        }
        lblName.text = film.title
        lblImdbRating.setCustomAttributedText(string1: film.imdbRating,
                                              font1: FontBook.semiBold.of(size: 14),
                                              color1: Constants.colorRed,
                                              string2: " (\(film.imdbVotes))",
                                              font2: FontBook.regular.of(size: 13),
                                              color2: Constants.colroDarkGray)
    }
}
