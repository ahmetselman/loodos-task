//
//  RoundedView.swift
//  loodos
//
//  Created by Ahmet Selman Durak on 15.02.2022.
//

import UIKit

class RoundedView: UIView {

    override func didMoveToSuperview() {
        self.layer.cornerRadius = 16
        self.layer.masksToBounds = true
    }

}
