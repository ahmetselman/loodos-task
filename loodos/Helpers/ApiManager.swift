//
//  ApiManager.swift
//  loodos
//
//  Created by Ahmet Selman Durak on 15.02.2022.
//

import Foundation
import Alamofire

enum ApiManagerErrors{
    case parameterMissing
    case noContent
    case parseError
}

struct ApiManager {

    
    static func searchMovieWith(imdbId: String?, title: String?, onCompetion: @escaping (_ result: Bool,_ response:Any?,_ error:ApiManagerErrors?) -> Void){
        
        //setting request parameters
        var parameters : [String : String] = [:]
        if let id = imdbId{
            parameters["i"] = id
        }
        else if let titleSearchString = title{
            parameters["t"] = titleSearchString
        }
        else {
            onCompetion(false, nil, .parameterMissing)
            return
        }
        parameters["apikey"] = Constants.apikey
        
        //connect to API and get error or response
        AF.request(Constants.endpoint, method: .get, parameters: parameters).validate().responseDecodable(of: Film.self){(response) in
            //check if any error came from server
            let errorString = hasError(data: response.data)
            if !errorString.isEmpty{
                onCompetion(false,errorString, .noContent)
                return
            }
            //check if an error with Alamofire model parsing or any error with Alamofire
            if response.error != nil {
                print(response.error.debugDescription)
                onCompetion(false, nil, .parseError)
                return
            }
            //get response of or film detail model
            guard let filmDetailResponse = response.value else {
                onCompetion(false, nil, .parseError)
                return
            }
            //send response to controller
            onCompetion(true, filmDetailResponse, nil)
            
        }
    }
    
    static func getTopTenMovies(onCompetion: @escaping (_ movies: [Film]) -> Void){
        let filmImbdIds = ["tt0111161", "tt0068646", "tt0468569", "tt0071562", "tt0050083", "tt0167260", "tt0110912", "tt0108052", "tt1375666", "tt0137523",]
        var movies : [Film] = []
        for movieId in filmImbdIds{
            self.searchMovieWith(imdbId: movieId, title: nil) { result, response, error in
                if result{
                    if let movie = response as? Film {
                        movies.append(movie)
                    }
                }
            }
        }
        onCompetion(movies)
        
    }
    
    static private func hasError(data:Data?) -> String {
        if let errorModel = try? JSONDecoder().decode(ErrorModel.self, from: data ?? Data()){
            return errorModel.errorMessage
        }
        else {
            return ""
        }
    }
}
