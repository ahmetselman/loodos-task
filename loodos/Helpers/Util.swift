//
//  Utils.swift
//  lidy
//
//  Created by Ahmet Selman Durak on 10.10.2021.
//

import Foundation
import UIKit

struct Util {
    
    static var isanimating = false
    static var lightBackgroundForLogo: UIView?
    static var lightBackgroundImageView: UIView?
    static var logoImageView: UIImageView?
    
    static func dateToString(date:Date, format:String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
    
    static func stringToDate(dateString : String)-> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter.date(from: dateString) ?? Date()
    }
    
    static func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    
    static func addLoadingLogoToView (_ parentView: UIView) {
        
        self.lightBackgroundForLogo = UIView(frame: parentView.bounds)
        self.lightBackgroundForLogo!.backgroundColor = UIColor.clear
        parentView.addSubview(self.lightBackgroundForLogo!)
        
        lightBackgroundImageView = nil
        self.lightBackgroundImageView = UIImageView(frame: parentView.bounds)
        self.lightBackgroundImageView!.backgroundColor = UIColor.white
        self.lightBackgroundImageView?.alpha = 0
        lightBackgroundForLogo?.addSubview(lightBackgroundImageView!)
        
        logoImageView = UIImageView(image: UIImage(named: "indicator"))
        logoImageView?.contentMode = .scaleAspectFit
        logoImageView?.frame = CGRect(x: 0, y: 0, width: parentView.bounds.size.width / 10.0, height: parentView.bounds.size.width / 10.0)
        self.logoImageView!.center = CGPoint(x: parentView.bounds.size.width / 2.0, y: parentView.bounds.size.height / 2.0)
        lightBackgroundForLogo?.addSubview(logoImageView!)

        let animation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        animation.duration = 1.0
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.repeatCount = .infinity
        animation.values = [0, Double.pi/2, Double.pi, Double.pi*3/2, Double.pi*2]
        //Percentage of each key frame
        let moments = [NSNumber(value: 0.0), NSNumber(value: 0.1),
                       NSNumber(value: 0.3), NSNumber(value: 0.8), NSNumber(value: 1.0)]
        animation.keyTimes = moments
        
        logoImageView!.layer.add(animation, forKey: "rotate")
        
        isanimating = true
        
    }
    
    static func removeLoadingLogoFromView(_ parentView : UIView) {
        
        if (self.logoImageView != nil && isanimating) {
            lightBackgroundForLogo?.removeFromSuperview()
            lightBackgroundImageView?.removeFromSuperview()
            logoImageView?.layer.removeAnimation(forKey: "rotate")
            logoImageView?.removeFromSuperview()
            lightBackgroundForLogo = nil
            logoImageView = nil
            isanimating = false
        }
        
        if (self.lightBackgroundForLogo != nil) {
            self.lightBackgroundForLogo!.removeFromSuperview()
        }
    }
    
    
    
    static func purifyPhoneNumber(phone:String) -> String{
        
        let purePhoneNumber = phone.replacingOccurrences(of: " ", with: "")
        var result = purePhoneNumber
        if purePhoneNumber.prefix(1) == "0" {
            result = String(purePhoneNumber.dropFirst())
        }
        return result
    }
    
    static func getToken() -> String {
        
        //return "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI3TUJ0UDJHUU1WYVRFQVRZM3RNbGx3PT0iLCJwaG9uZU51bWJlciI6IjF2dXRuMTltcTRFd3R5Sk84SHJscUE9PSIsInRva2VuVHlwZSI6IlJFRlJFU0giLCJleHAiOjE2NDk4NTkxNjYsImlhdCI6MTYzNDMwNzE2Nn0.LydSEIJCQ8_wZYOXC7oxuGrJTe9DTTAnVPzlhgWw_eTuIHPe82tFKH8EdYacO1JI3fBbkFmQfQLldS-NfSxZnA"
        
        let token = Constants.defaults.string(forKey: "token")
        if token == nil {
            return ""
        }
        return "Bearer " + token!
        
    }
    
    static var deviceModel: String {
        var utsnameInstance = utsname()
        uname(&utsnameInstance)
        let optionalString: String? = withUnsafePointer(to: &utsnameInstance.machine) {
            $0.withMemoryRebound(to: CChar.self, capacity: 1) {
                ptr in String.init(validatingUTF8: ptr)
            }
        }
        return optionalString ?? "N/A"
    }

    
    static func printDictionaryAsJsonString(dict : [String:Any]) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            
            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
            // here "decoded" is of type `Any`, decoded from JSON data
            print(decoded)
            
        } catch {
            print(error.localizedDescription)
        }
    }
    
    static func attirubuteString(string:String, rangeString:String, colorOfRangeString: UIColor, fontOfRangeString : UIFont?) -> NSMutableAttributedString {
        
        let range = (string as NSString).range(of: rangeString)
        let attributedString    = NSMutableAttributedString(string: string)
        
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: colorOfRangeString, range: range)
        
        if fontOfRangeString != nil {
            attributedString.addAttribute(NSAttributedString.Key.font, value: fontOfRangeString!, range: range)
        }
        
        return attributedString
    }
    
}
