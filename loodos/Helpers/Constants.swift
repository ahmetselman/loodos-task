//
//  Constants.swift
// 
//
//  Created by Ahmet Selman Durak on 8.10.2021.
//

import Foundation
import UIKit



struct Constants {
    
    static let defaults = UserDefaults.standard
    static let appDelegate =  UIApplication.shared.delegate as! AppDelegate
    
    //MARK: Colors
    static let colorLightGray = UIColor(hexString: "#4b4b4b")
    static let colroDarkGray = UIColor(hexString: "#3d3d3d")
    static let colorRed = UIColor(hexString: "#ff3838")
    static let colorPurple = UIColor(hexString: "#7158e2")
    
    //MARK: Strings
    static let noContnetAlertText = "We couldn't find any movies that matchs with your search"
    static let apiErrorAlertText = "Something went wrong. We are working on it.Please try again later"
    static let searchBoxEmptyAlertText = "Please enter a movie name"
    
    //MARK: Numbers
    static let splashWaitingTime = 3.0

    //MARK: Api constants
    static let endpoint = "http://www.omdbapi.com"
    static let apikey = "1072c19a"
}


