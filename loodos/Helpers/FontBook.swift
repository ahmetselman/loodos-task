//
//  FontBook.swift
//  loodos
//
//  Created by Ahmet Selman Durak on 14.02.2022.
//

import Foundation
import UIKit

enum FontBook: String {
    case bold = "Montserrat-Bold"
    case regular = "Montserrat-Regular"
    case medium = "Montserrat-Medium"
    case semiBold = "Montserrat-SemiBold"
    
    func of(size: CGFloat) -> UIFont {
        return UIFont(name: self.rawValue, size: size)!
    }
}
