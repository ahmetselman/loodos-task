//
//  Extensions.swift
//  loodos
//
//  Created by Ahmet Selman Durak on 14.02.2022.
//

import Foundation
import UIKit

extension UIViewController {
    func alert(title:String, alertBody: String, cancelButtonTitle:String? = nil , confirmButtonTitle : String ,dismissOnTap : Bool? = false, isFirstButtonBordered : Bool? = false, confirmTapped: (() -> Void)?, cancelTapped:(() -> Void)?) {
        let alertService = AlertService()
        let alertVc = alertService.alert(title: title, body: alertBody, actionButtonTitle: confirmButtonTitle, cancelButtonTitle: cancelButtonTitle, confirmButtonTapped: confirmTapped, cancelButtonTapped: cancelTapped)
        present(alertVc, animated: true)
    }
    
    func showSpinner() {
        self.view.isUserInteractionEnabled = false
        Utils.addLoadingSpinner(self.view)
    }
    func removeSpinner() {
        self.view.isUserInteractionEnabled = true
        Utils.removeLoadingSpiner(self.view)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}

extension UILabel{
    
    func setCustomAttributedText(string1:String, font1: UIFont,color1: UIColor, string2:String, font2:UIFont, color2:UIColor) {
        
        let atr1 = [NSAttributedString.Key.font:  font1, NSAttributedString.Key.foregroundColor: color1]
        let str1 = NSMutableAttributedString(string: string1, attributes: atr1 as [NSAttributedString.Key : Any])
        
        let atr2 = [NSAttributedString.Key.font: font2, NSAttributedString.Key.foregroundColor: color2]
        let str2 = NSAttributedString(string: string2, attributes: atr2 as [NSAttributedString.Key : Any])
        
        str1.append(str2)
        self.attributedText = str1
    }
}
