//
//  Film.swift
//  loodos
//
//  Created by Ahmet Selman Durak on 15.02.2022.
//

import Foundation

// MARK: - Film
struct Film: Codable, Jsonable {
    let title, year, rated, released: String
    let runtime, genre, director, writer: String
    let actors, plot, language, country: String
    let awards: String?
    let poster: String
    let ratings: [Rating]?
    let metascore, imdbRating, imdbVotes, imdbID: String
    let type, boxOffice, production: String?
    let website, response: String?
    let dvd : String?

    enum CodingKeys: String, CodingKey {
        case title = "Title"
        case year = "Year"
        case rated = "Rated"
        case released = "Released"
        case runtime = "Runtime"
        case genre = "Genre"
        case director = "Director"
        case writer = "Writer"
        case actors = "Actors"
        case plot = "Plot"
        case language = "Language"
        case country = "Country"
        case awards = "Awards"
        case poster = "Poster"
        case ratings = "Ratings"
        case metascore = "Metascore"
        case imdbRating, imdbVotes, imdbID
        case type = "Type"
        case dvd = "DVD"
        case boxOffice = "BoxOffice"
        case production = "Production"
        case website = "Website"
        case response = "Response"
    }

}

// MARK: - Rating
struct Rating: Codable {
    let source, value: String

    enum CodingKeys: String, CodingKey {
        case source = "Source"
        case value = "Value"
    }
}

protocol Jsonable {}

extension Jsonable {
    func toLogDict() -> [String:NSObject] {
        var dict = [String:NSObject]()
        let itSelf = Mirror(reflecting: self)
        for child in itSelf.children {
            if let key = child.label {
                dict[key] = child.value as? NSObject
            }
            //This code snippet is not regular for publish. But the reason is FirebaseLogging not accept above 100 chars. So i add only 5 key-value pair for logging movie detail
            if dict.count == 5 {
                break
            }
        }
        
       
        return dict
    }
}
