//
//  ErrorModel.swift
//  loodos
//
//  Created by Ahmet Selman Durak on 15.02.2022.
//

import Foundation

struct ErrorModel: Codable {
    let errorMessage, response : String
    
    enum CodingKeys: String, CodingKey {
        case errorMessage = "Error"
        case response = "Response"
    }
}
