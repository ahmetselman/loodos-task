//
//  Utils.swift
//
//  Created by Ahmet Selman Durak on 14.02.2022.
//

import Foundation
import UIKit
import FirebaseAnalytics

struct Utils {
    
    static var isanimating = false
    static var spinnerImageView: UIImageView?
    
    
    static func addLoadingSpinner (_ parentView: UIView) {
        if !isanimating {
            if (spinnerImageView == nil){
                spinnerImageView = UIImageView(image: UIImage(named: "film"))
                spinnerImageView?.contentMode = .center
                spinnerImageView?.frame = CGRect(x: 0, y: 0, width: parentView.bounds.size.width / 8.0, height: parentView.bounds.size.width / 8.0)
                spinnerImageView!.center = CGPoint(x: parentView.bounds.size.width / 2.0, y: parentView.bounds.size.height / 2.0)
                spinnerImageView?.backgroundColor = Constants.colorLightGray
                spinnerImageView?.layer.cornerRadius = spinnerImageView!.frame.size.height / 2
                parentView.addSubview(spinnerImageView!)
            }
            
            let animation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
            animation.duration = 1.0
            animation.fillMode = CAMediaTimingFillMode.forwards
            animation.repeatCount = .infinity
            animation.values = [0, Double.pi/2, Double.pi, Double.pi*3/2, Double.pi*2]
            //Percentage of each key frame
            let moments = [NSNumber(value: 0.0), NSNumber(value: 0.1),
                           NSNumber(value: 0.3), NSNumber(value: 0.8), NSNumber(value: 1.0)]
            animation.keyTimes = moments
            
            spinnerImageView!.layer.add(animation, forKey: "rotate")
            
            isanimating = true
        }
        
        
    }
    
    static func removeLoadingSpiner(_ parentView : UIView) {
        
        if ((spinnerImageView != nil) && isanimating) {
            spinnerImageView?.layer.removeAnimation(forKey: "rotate")
            spinnerImageView?.removeFromSuperview()
            isanimating = false
        }
    }
    
    static func showConnectionErrorAlert() {
        let alertService = AlertService()
        let alertVc = alertService.alert(title: "No connection!", body: "Please check your internet connection", actionButtonTitle: "OK")
        Constants.appDelegate.window!.rootViewController!.present(alertVc, animated: true)
    }
    static func showConnectionErrorAlert(title: String, alertString: String) {
        let alertService = AlertService()
        let alertVc = alertService.alert(title: title, body: alertString, actionButtonTitle: "OK")
        Constants.appDelegate.window!.rootViewController!.present(alertVc, animated: true)
    }
    
    static func FbLogEvent(parameters : [String : NSObject]) {
        Analytics.logEvent("movie_Detail", parameters: parameters)
    }
    
}
