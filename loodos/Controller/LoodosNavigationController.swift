//
//  LoodosNavigationController.swift
//  loodos
//
//  Created by Ahmet Selman Durak on 15.02.2022.
//

import UIKit

class LoodosNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.isNavigationBarHidden = true
    }
}
