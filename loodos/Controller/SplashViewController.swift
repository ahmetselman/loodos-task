//
//  ViewController.swift
//  loodos
//
//  Created by Ahmet Selman Durak on 14.02.2022.
//

import UIKit
import FirebaseRemoteConfig

class SplashViewController: UIViewController {
    
    @IBOutlet weak var lblSplash: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblSplash.alpha = 0
        self.view.backgroundColor = Constants.colorRed
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if connectionAvailable() {
            
            fetchRemoteConfig { success, remoteConfig in
                if success {
                    let splashLabelString = remoteConfig.configValue(forKey: "splashText").stringValue
                    self.lblSplash.text = splashLabelString
                    Shared.sharedInstance.appName = splashLabelString!
                    UIView.animate(withDuration: 0.5) {
                        self.lblSplash.alpha = 1
                    } completion: { completed in
                        if completed{
                            self.gotoNextScreen()
                        }
                    }
                }
            }
        }
    }
    //Check if connection available
    func connectionAvailable() -> Bool {
        if Constants.appDelegate.reachability.connection == .unavailable {
            Utils.showConnectionErrorAlert()
            return false
        }
        return true
    }
    
    func fetchRemoteConfig(onCompetion: @escaping (Bool, RemoteConfig) -> Void) {
        let remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.fetch(withExpirationDuration: 0) {(status, error) in
            guard error == nil else { onCompetion(false, remoteConfig); return }
            remoteConfig.activate()
            onCompetion(true, remoteConfig)
        }
    }
    
    func gotoNextScreen() {
        //spinner is showing for demo while waiting 3 seconds. Not have any function here
        showSpinner()
        DispatchQueue.main.asyncAfter(deadline: .now() + Constants.splashWaitingTime) {
            self.removeSpinner()
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "main") as! MainViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

}

