//
//  MainViewController.swift
//  loodos
//
//  Created by Ahmet Selman Durak on 15.02.2022.
//

import UIKit
import FirebaseRemoteConfig

class MainViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionView : UICollectionView!
    
    var mainArray : [Film] = []
    let filmImbdIds = ["tt0111161", "tt0068646", "tt0468569", "tt0071562", "tt0050083", "tt0167260", "tt0110912", "tt0108052", "tt1375666", "tt0137523"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //adds tap gesture to view to hide keyboard
        hideKeyboardWhenTappedAround()
        
        //set title label text which is coming from remote config and setted on singleton before
        lblTitle.text = Shared.sharedInstance.appName
        
        initCollectionView()
        customizeSearchBar()
        fetchAllFilms()

    }
    override func viewDidAppear(_ animated: Bool) {
        Constants.appDelegate.getPermissonForPushNotifications(application: UIApplication.shared)
    }
    
    func customizeSearchBar() {
        //customize searchbar
        searchBar.barTintColor = Constants.colorRed
        searchBar.barStyle = .black
        searchBar.isTranslucent = false
        searchBar.tintColor = Constants.colroDarkGray
        if #available(iOS 13.0, *) {
            searchBar.searchTextField.backgroundColor = .white
        } else {
            if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
                textfield.textColor = Constants.colroDarkGray
                textfield.backgroundColor = .white
            }
        }
        searchBar.delegate = self
    }
    
    func initCollectionView(){
        //setting collectionview parameters
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsVerticalScrollIndicator = false
        collectionView.backgroundColor = Constants.colorLightGray
        //give a little padding only top of collection view
        collectionView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
    }
    
    func fetchAllFilms()  {
        showSpinner()
        var filmArray : [Film] = []
        let fetchGroup = DispatchGroup()

        for id in self.filmImbdIds{
            fetchGroup.enter()
            ApiManager.searchMovieWith(imdbId: id, title: nil) { result, response, error in
                guard let filmdetail = response as? Film else {return}
                filmArray.append(filmdetail)
                fetchGroup.leave()
            }
            
        }
        fetchGroup.notify(queue: .main) {
            self.removeSpinner()
            self.mainArray = filmArray
            self.collectionView.reloadData()
        }
    }
    
    @IBAction func refreshTapped(_ sender:UIButton) {
        fetchAllFilms()
    }
}

//MARK: UICollectionview Delegate and Datasource
extension MainViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mainArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        //loading custom cell which has designed on storyboard
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MovieCollectionViewCell
        //finding next movie in array
        let nextMovie = mainArray[indexPath.row]
        //fill celll with movie info
        cell.fillCell(film: nextMovie)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedMovie = mainArray[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "detail") as! MovieDetailViewController
        vc.movie = selectedMovie
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //setting the size of uicollectionview cells
        let width = collectionView.bounds.width - 20
        return CGSize(width: width / 2 , height: collectionView.frame.size.height / 2.5)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        //for vertical spaces between cells
        return 20
    }
}
//MARK: UISearchbar delegate
extension MainViewController : UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text!.isEmpty{
            alert(title: "Opps!", alertBody: Constants.searchBoxEmptyAlertText, confirmButtonTitle: "OK", confirmTapped: nil, cancelTapped: nil)
            return
        }
        
        self.showSpinner()
        ApiManager.searchMovieWith(imdbId: nil, title: searchBar.text!) { result, response, error in
            self.removeSpinner()
            if result{
                guard let film = response as? Film else{
                    self.alert(title: "Sorry", alertBody: Constants.apiErrorAlertText, confirmButtonTitle: "OK", confirmTapped: nil, cancelTapped: nil)
                    return
                    
                }
                self.mainArray = [film]
                self.collectionView.reloadData()
            }
            else{
                switch error {
                case .noContent:
                    self.alert(title: "Sorry", alertBody: Constants.noContnetAlertText, confirmButtonTitle: "OK", confirmTapped: nil, cancelTapped: nil)
                case .parseError, .parameterMissing:
                    self.alert(title: "Sorry", alertBody: Constants.apiErrorAlertText, confirmButtonTitle: "OK", confirmTapped: nil, cancelTapped: nil)
                default:
                    break
                }
                
            }
        }
    }
    
    
}
