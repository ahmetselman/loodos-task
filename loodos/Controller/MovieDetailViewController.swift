//
//  MovieDetailViewController.swift
//  loodos
//
//  Created by Ahmet Selman Durak on 17.02.2022.
//

import UIKit
import Kingfisher
import Alamofire

class MovieDetailViewController: UIViewController {
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var tableView : UITableView!
    var movie : Film!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = movie.title
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let parameters = movie.toLogDict()
        Utils.FbLogEvent(parameters: parameters)
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .singleLine
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        tableView.backgroundColor = .clear
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension MovieDetailViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "imageCell")
            let imageview = cell?.viewWithTag(100) as! UIImageView
            imageview.contentMode = .scaleAspectFill
            if let imageUrl = URL(string: movie.poster){
                imageview.kf.setImage(with: imageUrl)
            }
            cell?.selectionStyle = .none
            cell?.backgroundColor = .clear
            return cell!
        }
        else if indexPath.row == 8 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "imdbCell")
            let label = cell?.viewWithTag(100) as! UILabel
            label.setCustomAttributedText(string1: "\(movie.imdbRating)",
                                          font1: FontBook.semiBold.of(size: 17),
                                          color1: Constants.colroDarkGray,
                                          string2: " Tap to see on IMDb",
                                          font2: FontBook.semiBold.of(size: 17),
                                          color2: Constants.colorRed)
            cell?.selectionStyle = .none
            return cell!
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! MovieDetailTableViewCell
            switch indexPath.row {
            case 1:
                cell.fillCell(iconname: "info", title: "Subject: ", detail: movie.plot)
            case 2:
                cell.fillCell(iconname: "genre", title: "Genre: ", detail: movie.genre)
            case 3:
                cell.fillCell(iconname: "calendar", title: "Year: ", detail: movie.year)
            case 4:
                cell.fillCell(iconname: "release", title: "Released: ", detail: movie.released)
            case 5:
                cell.fillCell(iconname: "director", title: "Director: ", detail: movie.director)
            case 6:
                cell.fillCell(iconname: "penn", title: "Writer: ", detail: movie.writer)
            case 7:
                cell.fillCell(iconname: "actor", title: "Actors: ", detail: movie.actors)
            default:
                break
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 8 {
            if let url = URL(string: "https://www.imdb.com/title/\(movie.imdbID)") {
                UIApplication.shared.open(url)
            }
        }
    }
}
