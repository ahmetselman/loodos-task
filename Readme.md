This project has preapared for Loodos Tech
Project has written with swift programming language. The aim is creating simple app to search for a movie and show the details to user.

PODS
- Alamofire (Networking)
- ReachabilitySwift (Checking internet connection)
- Firebase (Analytics, Remote Config, Push Notification)
- Kingfisher (Image load and caching)

INFO
- OMDB API has never returns an array of movies. It always returns a movie detail that you asked for API. So:
    In main screen i choose top ten movies and create a static imdb id array of these movies. In viewDidLoad i load top ten movies seperately
- While connecting API responses are really quick. So my own loading indicator can not visible on screen. I especially wait for some seconds to view the loading indicator.
- I have to reduce the size of movie detail for Firebase logging. Because Firebase accepts maximum 100 chars for logging. 

STRUCTURE
- MVC used for this tiny project
- Folder structure is simple
- No third party used for loading indicator and alert view. They are custom.
- Custom font used (Montserrat)
